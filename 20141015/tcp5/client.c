#include "sysutil.h"
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)

void do_service(int sockfd);

int main(int argc, const char *argv[])
{
    int peerfd = tcp_client(9999);
    connect_host(peerfd, "192.168.44.136", 8976);

    printf("%s connected\n", get_tcp_info(peerfd));

    do_service(peerfd);

    close(peerfd);
    return 0;
}


void do_service(int sockfd)
{
#define SIZE 1024
    char sendbuf[SIZE + 1] = {0};

    int cnt = 0; //次数
    while(1)
    {
        int i;
        for(i = 0; i < 10; ++i)
        {
            send_msg_with_len(sockfd, sendbuf, SIZE);
            
            printf("count = %d, write %d bytes\n", ++cnt, SIZE);
        }
        nano_sleep(2.5);
    }
}

