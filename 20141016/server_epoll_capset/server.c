#include "sysutil.h"
#include <signal.h>
#include <sys/epoll.h>
#include <linux/capability.h>
#include <sys/syscall.h>
#include <pwd.h>

#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)

void do_service(int sockfd);

int capset(cap_user_header_t hdrp, const cap_user_data_t datap)
{
    //#define SYS_capset __NR_capset
    return syscall(__NR_capset, hdrp, datap);
}

void set_nobody()
{
    //基本思路
    //1.首先获取nobody的uid、gid
    //2.然后逐项进行设置
    struct passwd *pw;
    if((pw = getpwnam("nobody")) == NULL)
        ERR_EXIT("getpwnam");

    if(setegid(pw->pw_gid) == -1)
        ERR_EXIT("setegid");

    if(seteuid(pw->pw_uid) == -1)
        ERR_EXIT("seteuid");

}

void set_bind_capabilities()
{
    struct __user_cap_header_struct cap_user_header;
    cap_user_header.version = _LINUX_CAPABILITY_VERSION_1;
    cap_user_header.pid = getpid();

    struct __user_cap_data_struct cap_user_data;
    __u32 cap_mask = 0; //类似于权限的集合
    cap_mask |= (1 << CAP_NET_BIND_SERVICE); //0001000000
    cap_user_data.effective = cap_mask;
    cap_user_data.permitted = cap_mask;
    cap_user_data.inheritable = 0; //子进程不继承特权

    if(capset(&cap_user_header, &cap_user_data) == -1) 
        ERR_EXIT("capset");
}

//以root权限启动程序
int main(int argc, const char *argv[])
{
    set_nobody();
    set_bind_capabilities();

    int listenfd = tcp_server("192.168.44.136", 21);

    //创建epoll
    int epoll_fd = epoll_create(2048);
    if(epoll_fd == -1)
        ERR_EXIT("epoll_create");
    //初始化
    struct epoll_event ev;
    ev.data.fd = listenfd;
    ev.events = EPOLLIN;
    if(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listenfd, &ev) == -1)
        ERR_EXIT("epoll add listenfd");
    //创建数组用于接收结果
    struct epoll_event events[2048];

    //epoll
    int nready;
    while(1)
    {
        //wait
        nready = epoll_wait(epoll_fd, events, sizeof events, 5000);
        if(nready == -1)
        {
            if(errno == EINTR)
                continue;
            ERR_EXIT("epoll wait");
        }
        else if(nready == 0)
        {
            printf("epoll timeout....\n");
            continue;
        }

        //遍历events数组 nready代表长度
        int i;
        for(i = 0; i < nready; ++i)
        {
            int fd = events[i].data.fd;
            if(fd == listenfd) //监听套接字
            {
                if(events[i].events & EPOLLIN)
                {
                    int peerfd = accept(fd, NULL, NULL);
                    //加入epoll
                    struct epoll_event ev;
                    ev.data.fd = peerfd;
                    ev.events = EPOLLIN; //一定加入事件
                    if(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, peerfd, &ev) == -1)
                        ERR_EXIT("epoll add clients");
                }
            }
            else //普通fd
            {
                if(events[i].events & EPOLLIN)
                {
                    char recvbuf[1024] = {0};
                    size_t nread = recv_msg_with_len(fd, recvbuf, sizeof recvbuf);
                    if(nread == 0)  //close
                    {
                        //从epoll中删除
                        struct epoll_event ev;
                        ev.data.fd = fd;
                        if(epoll_ctl(epoll_fd, EPOLL_CTL_DEL, fd, &ev) == -1)
                            ERR_EXIT("epoll delete clients");
                        close(fd);
                        continue;
                    }
                    printf("receive msg : %d\n", nread);
                    send_msg_with_len(fd, recvbuf, nread); 
                }
            }
        }


    }

    close(listenfd);

    return 0;
}
