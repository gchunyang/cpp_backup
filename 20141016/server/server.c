#include "sysutil.h"
#include <signal.h>
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)

void do_service(int sockfd);


int main(int argc, const char *argv[])
{
    int listenfd = tcp_server("192.168.44.136", 8976);

    while(1)
    {
        int peerfd = accept(listenfd, NULL, NULL);
        if(peerfd == -1)
            ERR_EXIT("accept");

        printf("%s connected\n", get_tcp_info(peerfd));

        do_service(peerfd);
    }

    

    close(listenfd);

    return 0;
}

void do_service(int sockfd)
{
    char recvbuf[1024] = {0};

    while(1)
    {
        size_t nread = recv_msg_with_len(sockfd, recvbuf, sizeof recvbuf);
        if(nread == 0)
        {
            printf("close ...\n");
            close(sockfd);
            break;
        }
        printf("receive msg : %d\n", nread);
        send_msg_with_len(sockfd, recvbuf, nread);
    }
}



