#include "Timer.h"
#include "Thread.h"
#include <stdio.h>
#include <unistd.h>

void foo()
{
    printf("foo\n");
}


Timer *g_timer = NULL;

void anotherThread()
{
    sleep(10);
    g_timer->stop();
}


int main(int argc, char const *argv[])
{
    Timer t(3, 1, &foo);
    g_timer = &t;


    Thread thd(&anotherThread);
    thd.start();

    t.start();

    thd.join();
    return 0;

}