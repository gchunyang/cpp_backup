#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)

void sigalrm_handler(int sig)
{
    printf("foo\n");
    alarm(1);
}


int main(int argc, const char *argv[])
{
    if(signal(SIGALRM, sigalrm_handler) == SIG_ERR)
        ERR_EXIT("signal");

    alarm(3); //一次有效


    for(;;)
        pause();

    return 0;
}
