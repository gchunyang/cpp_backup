#include "TimerThread.h"
#include <stdio.h>
#include <unistd.h>

void foo()
{
    printf("foo\n");
}



int main(int argc, char const *argv[])
{
    TimerThread t(3, 1, &foo);

    t.start();

    sleep(10);
    
    t.stop();

    return 0;

}