#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)

int main(int argc, const char *argv[])
{
    //int fd = open("test.txt", O_WRONLY | O_CREAT | O_TRUNC);
    int fd = open("test.txt", O_WRONLY | O_CREAT | O_APPEND);
    if(fd == -1)
        ERR_EXIT("open txt");
    write(fd, "foo", 3);
    write(fd, "bar", 3);
    close(fd);
    return 0;
}
