#include "Thread.h"
#include <stdio.h>
#include <unistd.h>
using namespace std;

void foo()
{
    while(1)
    {
        printf("foo\n");
        sleep(1);
    }
}



int main(int argc, char const *argv[])
{
    Thread t(&foo);

    t.start();
    t.join();

    return 0;
}