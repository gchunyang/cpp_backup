#include <iostream>
#include <string>
#include <vector>
using namespace std;

//生成pair对象的三种方法
int main(int argc, const char *argv[])
{
    vector<pair<string, int> > vec;

    pair<string, int> word;
    word.first = "hello";
    word.second = 12;
    vec.push_back(word);

    pair<string, int> word2("world", 12);
    vec.push_back(word2);
    
    vec.push_back(make_pair<string, int>("foo", 3));


    return 0;
}
