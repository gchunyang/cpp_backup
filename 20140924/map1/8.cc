#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
using namespace std;

struct Test
{
    int a;
};

int main(int argc, const char *argv[])
{

    map<int, Test> m_0;
    m_0[1] = Test();

    map<Test, int> m;  
    Test t;
    m[t] = 1;
    return 0;
}
