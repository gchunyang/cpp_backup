#include <iostream>
#include <string>
using namespace std;


class People
{
public:
    People() 
    {
        cout << "People()" << endl;
    }
    People(const string &name)
    : name_(name)
    {
        cout << "People(const string &name)" << endl;
    }
    People(string &&name)
    : name_(name)
    {
        cout << "People(string &&name)" << endl;
    }

    People(const People &p)
    : name_(p.name_)
    {
        cout << "People(const People &p)" << endl;
    }
    People(People &&p)
    : name_(p.name_)
    {
        cout << "People(People &&p)" << endl;
    }


private:
    string name_;
};

int main(int argc, char const *argv[])
{
    People p1("hello"); //move
    string s;
    People p2(s);

    People p3(p2);
    People p4(std::move(p3));
    //People p4(People());
    return 0;
}