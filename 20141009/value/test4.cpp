#include <iostream>
#include <string>
using namespace std;


string one("one");
const string two("two");
string three() { return "three"; }
const string four() { return "four"; }

void test(const string &s)
{
    cout << "test(const string &s):" << s << endl;
}

void test(string &s)
{
    cout << "test(string &s):" << s << endl;
}

void test(string &&s)
{
    cout << "test(string &&s):" << s << endl;
}

void test(const string &&s)
{
    cout << "test(const string &&s):" << s << endl;
}


int main(int argc, char const *argv[])
{
    

    test(one);
    test(two);
    test(three());
    test(four());


    return 0;
}