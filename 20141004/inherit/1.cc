#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Person
{
    public:
        Person() :id_(0), name_(""), age_(0) { }
        Person(int id, const string &name, int age)
            :id_(id), name_(name), age_(age)
        {   }

        void print() const
        {
            cout << "id = " << id_ << " name = " << name_ << "age = " << age_ << endl;
        }

    protected:
        int id_;
        string name_;
        int age_;
};


class Student : public Person
{
    public:
        void test()
        {
            cout << "test" << endl;
        }
    private:
        string school_;
};


int main(int argc, const char *argv[])
{
    Student s;
    s.print();

    s.test();

    return 0;
}







