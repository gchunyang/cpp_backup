#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Parent
{
    public:
        Parent() { cout << "Parent" << endl; }
        ~Parent() { cout << "~Parent" << endl; }
};

class Child : public Parent
{
    public:
        Child() { cout << "Child" << endl;}
        ~Child() { cout << "~Child " << endl;}
};

int main(int argc, const char *argv[])
{
    Child d;
    return 0;
}





