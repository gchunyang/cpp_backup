#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Person
{
    public:
        Person() :id_(0), name_(""), age_(0) { }
        Person(int id, const string &name, int age)
            :id_(id), name_(name), age_(age)
        {   }

        void print() const
        {
            cout << "id = " << id_ << " name = " << name_ << "age = " << age_ << endl;
        }
    protected:
        int id_;
        string name_;
        int age_;
};


class Student : public Person
{
    public:
        Student() :school_("") {    }
        Student(int id, const string &name, int age, const string &school)
            :Person(id, name, age), school_(school)
        {
            
        }

        Student(const Student &s)
            :Person(s), school_(s.school_)
        {
        
        }

        Student &operator=(const Student &s)
        {
            if(this != &s)
            {
                //先对基类对象赋值
                //再对自身变量赋值
                Person::operator=(s);
                school_ = s.school_;
            }

            return *this;
        }

        void print() const
        {
            Person::print();
            cout << "school = " << school_ << endl;
        } 

        void print(ostream &os) const
        {
            os << "school = " << school_ << endl;
        }

        void test()
        {
            cout << "test" << endl;
        }
    private:
        string school_;
};


int main(int argc, const char *argv[])
{
    Student s(12, "zhangsan", 23, "shiyan");

    Person *p1 = &s;
    Person &p2 = s;

    p1->print(); 
    //p1->test();//ERROR


    p1->print(cout);

    return 0;
}







