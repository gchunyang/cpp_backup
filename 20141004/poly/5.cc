#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Animal
{
    public:
        virtual void run()
        {
            cout << "run ..." << endl;
        }
};


class Cat : public Animal
{
    public:
        void run()
        {
            cout << "喵...." << endl;
        }
};

class Dog : public Animal
{
    public:
        void run()
        {
            cout << "汪...." << endl;
        }
};


int main(int argc, const char *argv[])
{
    Cat c;
    Dog d;


    Animal *pa = &c;
    pa->run();

    pa = &d;
    pa->run();


    return 0;
}










