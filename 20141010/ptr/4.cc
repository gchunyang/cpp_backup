#include <iostream>
#include <memory>
#include <vector>
using namespace std;

class Test
{
public:
    Test() { cout << "Test" << endl;}
    ~Test() { cout << "~Test" << endl;}

private:
    Test(const Test &);
    void operator=(const Test &);
};

int main(int argc, char const *argv[])
{
    vector<unique_ptr<Test> > coll;
    coll.push_back(unique_ptr<Test>(new Test));
    return 0;
}
