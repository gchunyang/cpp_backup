#include <memory>
#include <iostream>
#include <string>
using namespace std;

class Parent;
class Child;

typedef shared_ptr<Parent> ParentPtr;
typedef shared_ptr<Child> ChildPtr;

class Parent
{
public:
    ~Parent() { cout << "~Parent" << endl; }

    ChildPtr child_;
};

class Child
{
public:
    ~Child() { cout << "~Child" << endl;}

    ParentPtr parent_;
};


int main(int argc, char const *argv[])
{
    {
        ParentPtr parent(new Parent);
        ChildPtr child(new Child);
        parent->child_ = child;
        child->parent_ = parent;

        cout << "parent count: " << parent.use_count() << endl;
        cout << "child count: " << child.use_count() << endl;

    }
    return 0;
}
