#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main(int argc, char const *argv[])
{
    vector<string> coll;
    coll.push_back("foo");
    coll.push_back("bar");
    coll.push_back("foo");
    coll.push_back("foo");
    coll.push_back("foo");
    coll.push_back("foo");


    //for(vector<string>::const_iterator it = coll.begin();
    for(auto it = coll.begin();
        it != coll.end();
        ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    for(const auto &i : coll)
    {
        cout << i << " ";
    }
    cout << endl;


    return 0;
}