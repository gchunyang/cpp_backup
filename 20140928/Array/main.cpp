#include "Array.h"
#include <iostream>
#include <assert.h>
using namespace std;

int main(int argc, char const *argv[])
{

    Array a1(20);
    assert(a1.getSize() == 20);

    int arr[5] = {90, 45, 23, 45, 32};
    size_t size = sizeof(arr)/sizeof(arr[0]);
    Array a2(arr, size);
    assert(a2.getSize() == size);
    cout << a2 << endl;
    assert(a2[2] == 23);


    Array a3(a2);
    assert(a3.getSize() == size);
    cout << a3 << endl;
    assert(a3[2] == 23);
    a3[1] = 99;
    assert(a3[1] == 99);

    Array a4(100);
    a4 = a3;
    cout << a4 << endl;
    assert(a4.getSize() == size);

    a4 += a2;
    cout << a4 << endl;
    assert(a4.getSize() == 2 * size);


    int arr2[3] = {99, 88, 34};
    Array a5(arr2, 3);
    cout << a5 << " + " << a2 << " = " << (a5 + a2) << endl;
    a5 = a5 + a2;
    cout << a5 << endl;
    assert(a5.getSize() == 8);

    //a2 {90, 45, 23, 45, 32};
    // a5 [99, 88, 34, 90, 45, 23, 45, 32]

    assert(a2 < a5);
    assert(a2 <= a5);
    assert(a5 > a2);
    assert(a5 >= a2);
    assert(!(a2 == a5));
    assert(a2 != a5);

    Array a6(a5);
    assert(a6 == a5);


    return 0;
}

