#include "Array.h"
#include <string.h>
#include <stdexcept>
using namespace std;

Array::Array(size_t size)
    :data_(new int[size]),
     size_(size)
{
}


Array::Array(int *arr, size_t size)
    :data_(new int[size]),
     size_(size)
{
    ::memcpy(data_, arr, size * sizeof(int));
    //copy
}


Array::Array(const Array &other)
    :data_(new int[other.size_]),
     size_(other.size_)
{
    ::memcpy(data_, other.data_, size_ * sizeof(int));
}


Array::~Array()
{
    delete[] data_;
}


ostream &operator<<(ostream &os, const Array &arr)
{
    //[1, 2, 3]
    os << "[";
    for(size_t ix = 0; ix != arr.size_; ++ix)
    {
        os << arr.data_[ix];
        if(ix != arr.size_ - 1)
            os << ", ";
    }
    os << "]";

    return os;
}

// 3 4 5 2 1 0 0 0 0 0  // 10 5

Array &Array::operator=(const Array &other)
{
    if(this != &other)
    {
        delete[] data_;
        data_ = new int[other.size_];
        size_ = other.size_;
        ::memcpy(data_, other.data_, size_ * sizeof(int));
    }

    return *this;
}

Array & Array::operator+=(const Array &other)
{
    int *data = new int[size_ + other.size_];
    ::memcpy(data, data_, size_ * sizeof(int));
    ::memcpy(data + size_, other.data_, other.size_ * sizeof(int));

    delete data_;
    data_ = data;
    size_ += other.size_;

    return *this;
}


int &Array::operator[] (size_t index)
{
    if(index >= size_)
        throw out_of_range("越界!");
    return data_[index];
}

int Array::operator[] (size_t index) const
{
    if(index >= size_)
        throw out_of_range("越界!");
    return data_[index];
}


Array operator+(const Array &a, const Array &b)
{
    Array ret(a);
    ret += b;

    return ret;
}



int Array::compare(const Array &other) const
{
    //strcmp
    size_t ix = 0;

    int ret;
    size_t size = (size_ < other.size_) ? size_ : other.size_; 
    while(ix != size)
    {
        ret = data_[ix] - other.data_[ix];
        if(ret != 0)
            return ret;
        ++ix;
    }

    if(ix != size_)
        return 1;
    if(ix != other.size_)
        return -1;

    return 0;
}


bool operator< (const Array &a, const Array &b)
{
    return a.compare(b) < 0;
}

bool operator<=(const Array &a, const Array &b)
{
    // a <= b -> !(a > b)
    return a.compare(b) <= 0;
}

bool operator> (const Array &a, const Array &b)
{
    return a.compare(b) > 0;
}

bool operator>=(const Array &a, const Array &b)
{
    return a.compare(b) >= 0;
}

bool operator==(const Array &a, const Array &b)
{
    return a.compare(b) == 0;
}

bool operator!=(const Array &a, const Array &b)
{
    return !(a == b);
}
