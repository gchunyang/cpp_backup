#ifndef ARRAY_H_
#define ARRAY_H_

#include <iostream>

class Array
{
    friend std::ostream &operator<<(std::ostream &os, const Array &arr);
    friend Array operator+(const Array &a, const Array &b);
    friend bool operator< (const Array &a, const Array &b);
    friend bool operator<=(const Array &a, const Array &b);
    friend bool operator> (const Array &a, const Array &b);
    friend bool operator>=(const Array &a, const Array &b);
    friend bool operator==(const Array &a, const Array &b);
    friend bool operator!=(const Array &a, const Array &b);

public:

    Array(size_t size);
    Array(int *arr, size_t size);
    Array(const Array &other);
    ~Array();

    Array &operator=(const Array &other);
    Array &operator+=(const Array &other);

    int &operator[] (size_t index);
    int operator[] (size_t index) const;

    size_t getSize() const
    { return size_; }

private:

    //strcmp
    int compare(const Array &other) const;

    int *data_;
    size_t size_; //元素数量
};


#endif /* ARRAY_H_ */