#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Animal
{
    public:
        virtual ~Animal() { }
        virtual void run() { cout << "test" << endl; }
};

class Cat : public Animal
{
    public:
        void run()
        {
            cout << "miao" << endl;
        }
};


class Dog : public Animal
{

    public:
        void run()
        {
            cout << "wang" << endl;
        }
};




int main(int argc, const char *argv[])
{
    Cat c;
    Dog d;
    Animal *pa = &c;

    Cat *pc = static_cast<Cat*>(pa);
    Dog *pd = static_cast<Dog*>(pa);
    pd->run();






    return 0;
}
