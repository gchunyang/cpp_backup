#include <iostream>
#include <string>
#include <vector>
#include <typeinfo>
using namespace std;

class Animal
{
    public:
        virtual ~Animal() { }
        virtual void run() { cout << "test" << endl; }
};

class Cat : public Animal
{
    public:
        void run()
        {
            cout << "miao" << endl;
        }
};


class Dog : public Animal
{

    public:
        void run()
        {
            cout << "wang" << endl;
        }
};




int main(int argc, const char *argv[])
{
    Cat c;
    Dog d;

    cout << typeid(string).name() << endl;

    cout << typeid(c).name() << endl;
    cout << typeid(d).name() << endl;

    Animal *pa = &c;
    cout << typeid(*pa).name() << endl;



    return 0;
}
