#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Animal
{
    public:
        virtual ~Animal() { }
        virtual void run() { cout << "test" << endl; }
};

class Cat : public Animal
{
    public:
        void run()
        {
            cout << "miao" << endl;
        }
};


class Dog : public Animal
{

    public:
        void run()
        {
            cout << "wang" << endl;
        }
};




int main(int argc, const char *argv[])
{
    Cat c;
    Dog d;
    Animal &pa = c;

    Cat &pc = dynamic_cast<Cat&>(pa);
    cout << &pc << endl;
    Dog &pd = dynamic_cast<Dog&>(pa);
    cout << &pd << endl;





    return 0;
}
