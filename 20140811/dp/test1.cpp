#include <iostream>
#include <string>
#include <vector>
#include <string.h>
#include <assert.h>
using namespace std;

inline int minThribble(int a, int b, int c)
{
    int ret = a < b ? a : b;
    ret = ret < c ? ret : c;
    return ret;
}

int dis(const string &a, const string &b)
{
    assert(a.size() < 100 && b.size() < 100);
    int memo[100][100];
    memset(memo, 0, sizeof memo);
    //L(i, 0) = i
    for(int i = 0; i <= a.size(); ++i)
        memo[i][0] = i;
    for(int j = 0; j <= b.size(); ++j)
        memo[0][j] = j;

    for(int i = 1; i <= a.size(); ++i)
    {
        for(int j = 1; j <= b.size(); ++j)
        {
            if(a[i-1] == b[j-1])
            {
                memo[i][j] = memo[i-1][j-1];
            }
            else
            {
                int t1 = memo[i-1][j];
                int t2 = memo[i][j-1];
                int t3 = memo[i-1][j-1];
                memo[i][j] = minThribble(t1, t2, t3) + 1;
            }
        }
    }

    return memo[a.size()][b.size()];
}


int main(int argc, const char *argv[])
{
    cout << "distance : " << dis(argv[1], argv[2]) << endl; 
    return 0;
}




