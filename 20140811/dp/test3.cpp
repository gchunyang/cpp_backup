#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
using namespace std;


int main(int argc, const char *argv[])
{
    priority_queue<int, vector<int>, greater<int> > q;
    q.push(12);
    q.push(13);
    q.push(9);

    while(!q.empty())
    {
        cout << q.top() << endl;
        q.pop();
    }
    return 0;
}
