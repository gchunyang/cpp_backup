#include <iostream>
#include <string>
#include <functional>
using namespace std;

class Foo
{
public:

    //void (Foo::*)(int)
    void foo(int a)
    {
        cout << a << endl;
    }

    //void (*)(int)
    static void bar(int a)
    {
        cout << a << endl;
    }
};


int main(int argc, char const *argv[])
{
    Foo f;

    //void (Foo::*)(int) -> void (*)(Foo*, int)


    (mem_fun(&Foo::foo))(&f, 123);

    return 0;
}

