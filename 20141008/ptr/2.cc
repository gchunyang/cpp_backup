#include <iostream>
#include <string>
#include <typeinfo>
using namespace std;

class Foo
{
public:

    //void (Foo::*)(int)
    void foo(int a)
    {
        cout << a << endl;
    }

    //void (*)(int)
    static void bar(int a)
    {
        cout << a << endl;
    }
};


int main(int argc, char const *argv[])
{
    //void (*pFunc)(int) = &Foo::foo;

    //pFunc(123);
    //cout << typeid(&Foo::foo).name() << endl;

    void (*pFunc)(int) = &Foo::bar;
    pFunc(123);


    void (Foo::*pFunc2)(int) = &Foo::foo;
    //pFunc2(234);
    Foo f;
    (f.*pFunc2)(45678);
    Foo *pf = &f;
    (pf->*pFunc2)(7865);

    return 0;
}

