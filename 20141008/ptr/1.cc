#include <iostream>
#include <string>
using namespace std;

void foo(int a)
{
    cout << a << endl;
}


int main(int argc, char const *argv[])
{
    void (*pFunc)(int) = &foo;
    pFunc(123);
    return 0;
}