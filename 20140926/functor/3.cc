#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Test
{
    public:

        void operator() ()
        {
            cout << "Hello World" << endl;
        }

        void operator() (int i)
        {
            cout << "Hello " << i << endl;
        }
};

int main(int argc, const char *argv[])
{
    Test t;
    t();

    t(99);
    return 0;
}
