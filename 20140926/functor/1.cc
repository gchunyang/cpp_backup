#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;


bool isGreater60(int i)
{
    return i >= 60;
}


int main(int argc, const char *argv[])
{
    vector<int> vec;

    vec.push_back(99);
    vec.push_back(87);
    vec.push_back(59);
    vec.push_back(43);
    vec.push_back(78);
    vec.push_back(91);


    int cnt = 
        std::count_if(vec.begin(), vec.end(), 
                [](int i){return i >= 80;});
    cout << cnt << endl;


    return 0;
}

