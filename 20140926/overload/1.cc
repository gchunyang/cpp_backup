#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
using namespace std;

class Test
{
    public:

        Test() { cout << "Test " << endl; }
        ~Test() { cout << "Test ...." << endl; }
};

int main(int argc, const char *argv[])
{
    try
    {
        Test *pt = new Test;
        Test t;
        throw runtime_error("错误");

        delete pt;

    }
    catch(...)
    {
        cout << "Catch ..." << endl; 
    }
    return 0;
}
