#include <iostream>
#include <string>
#include <vector>
#include <sstream>
using namespace std;

int main(int argc, const char *argv[])
{
    string s = "hello world foo bar";
    istringstream line(s); //用s去初始化一个字符串流

    string word;
    while(line >> word)
        cout << word << endl;

    return 0;
}
