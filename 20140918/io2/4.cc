#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include <fstream>
#include <algorithm>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

struct Word
{
    string word_;
    int count_;
};

void readFile(ifstream &in, vector<Word> &words);
void erasePunct(string &s);
void addWordToDict(vector<Word> &words, const string &word);
void printWordFrequency(const vector<Word> &words);
void sortWordsByFrequency(vector<Word> &words);

int main(int argc, const char *argv[])
{
    //打开文件
    //读取单词
    //打印个数

    if(argc < 2)
    {
        fprintf(stderr, "Usage:%s filename\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    ifstream infile(argv[1]);
    if(!infile)
        throw std::runtime_error("open file error");

    vector<Word> words;
    readFile(infile, words);

    sortWordsByFrequency(words);

    printWordFrequency(words);
    return 0;
}

void readFile(ifstream &in, vector<Word> &words)
{
    words.clear();
    string word;
    while(in >> word)
    {
        erasePunct(word);
        //words.push_back(word);
        addWordToDict(words, word);
    }
}

void erasePunct(string &s)
{
    string::iterator it = s.begin();
    while(it != s.end())
    {
        if(ispunct(*it))
            it = s.erase(it);
        else
            ++it;
    }
}

void addWordToDict(vector<Word> &words, const string &word)
{
    vector<Word>::iterator it = words.begin();

    while(it != words.end())
    {
        if(word == it->word_)
        {
            ++it->count_;
            break;
        }
        ++it;
    }

    if(it == words.end())
    {
        Word tmp;
        tmp.word_ = word;
        tmp.count_ = 1;
        words.push_back(tmp);
    }
}

void printWordFrequency(const vector<Word> &words)
{
    for(vector<Word>::const_iterator it = words.begin(); 
        it != words.end(); 
        ++it)
    {
        printf("word :%s, frequency: %d\n", it->word_.c_str(), it->count_);
    }
}

static bool cmp(const Word &w1, const Word &w2)
{
    //a > b
    return w1.count_ > w2.count_;
}

void sortWordsByFrequency(vector<Word> &words)
{
    sort(words.begin(), words.end(), cmp);
}













