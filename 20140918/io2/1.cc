#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)
using namespace std;

int main(int argc, const char *argv[])
{
    ofstream outfile;
    outfile.open("out.txt");
    if(!outfile)
    {
        fprintf(stderr, "open file error\n");
        exit(EXIT_FAILURE);
    }

    int a = 10;
    double b = 99;
    string s = "hello";
    outfile << a << endl << b << endl << s << endl;

    outfile.close();

    return 0;
}
