#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include <fstream>
#include <stdlib.h>
using namespace std;

void copyContent(ifstream &in, ofstream &out)
{
    string line;
    while(getline(in, line))
    {
        out << line << "\n";
    }
}

int main(int argc, const char *argv[])
{
    if(argc < 3)
    {
         fprintf(stderr, "Usage:%s src dest\n", argv[0]);
         exit(EXIT_FAILURE);
    }
    ifstream infile(argv[1]);
    ofstream outfile(argv[2]);
    if(!infile || !outfile)
        throw runtime_error("open file error");

    copyContent(infile, outfile);

    infile.close();
    outfile.close();
    return 0;
}

