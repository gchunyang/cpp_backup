#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)
using namespace std;

void writeIntegerToFile(int fd, int value)
{
    char text[100] = {0};
    snprintf(text, sizeof text, "%d\n", value);
    if(write(fd, text, strlen(text)) == -1)
        ERR_EXIT("write");
}

//返回s
int64_t get_time()
{
    struct timeval tm;
    memset(&tm, 0, sizeof tm);
    if(gettimeofday(&tm, NULL) == -1)
        ERR_EXIT("gettimeofday");
    int64_t t = 0;
    t += tm.tv_sec * 1000 * 1000;
    t += tm.tv_usec;
    return t;
}

int main(int argc, const char *argv[])
{
    if(argc < 3)
    {
        fprintf(stderr, "Usage %s count filename\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int64_t startTime = get_time();
    int kSize = atoi(argv[1]);
    srand(kSize);
    int fd = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0666);
    if(fd == -1)
        ERR_EXIT("open");


    for(int i = 0; i != kSize; ++i)
    {
        writeIntegerToFile(fd, rand() % kSize); 
    }

    close(fd);

    int64_t endTime = get_time();
    int64_t cost = endTime - startTime;
    cout << "花费时间 " << (cost/1000) << " ms" << endl;
    return 0;
}
