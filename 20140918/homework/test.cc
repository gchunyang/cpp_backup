#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
using namespace std;
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)

void readFile(const string &filename, vector<int> &vec);
bool binarySearch(const vector<int> &vec, int key);
bool simpleSearch(const vector<int> &vec, int key);
void findAndPrint(const vector<int> &white, const vector<int> &data);
int64_t getTime();

int main(int argc, const char *argv[])
{
    //1.读取白名单到vector中
    //2.对white进行排序
    //3.读取测试数据
    //4.遍历测试数据
    if(argc < 3)
    {
        fprintf(stderr, "Usage %s: whiteFile testFile\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int64_t startTime = getTime();

    string whiteFile = argv[1];
    string testFile = argv[2]; 
    vector<int> white;
    vector<int> testCases;
    readFile(whiteFile, white);
    readFile(testFile, testCases);

    sort(white.begin(), white.end());
    
    findAndPrint(white, testCases); 

    int64_t endTime = getTime();
    int64_t cost = endTime - startTime;
    cout << "白名单大小: " << white.size() 
           << "测试数据大小: " << testCases.size()
           << "花费时间: " << (cost/1000) << " ms" << endl;
    
    return 0;
}



void readFile(const string &filename, vector<int> &vec)
{
    vec.clear();
    FILE *fp = ::fopen(filename.c_str(), "r");
    if(fp == NULL)
    {
        fprintf(stderr, "open %s file error\n", filename.c_str());
        exit(EXIT_FAILURE);
    }

    char buf[1024];
    while(::fgets(buf, sizeof buf, fp) != NULL)
    {
        int num = atoi(buf);
        vec.push_back(num);
    }

    ::fclose(fp);
}

bool binarySearch(const vector<int> &vec, int key)
{
    int left = 0;
    int right = vec.size() - 1;

    int middle;
    while(left <= right)
    {
        middle = (left + right) / 2;
        if(vec[middle] == key)
            return true;
        else if(vec[middle] < key)
            left = middle + 1; //去右半范围查找
        else
            right = middle - 1;
    }

    return false;
}


bool simpleSearch(const vector<int> &vec, int key)
{
    for(vector<int>::size_type ix = 0;
        ix != vec.size();
        ++ix)
    {
        if(vec[ix] == key)
            return true;
    }

    return false;
}


void findAndPrint(const vector<int> &white, const vector<int> &data)
{
    for(vector<int>::const_iterator it = data.begin();
        it != data.end();
        ++it)
    {
        if(!simpleSearch(white, *it))
            printf("%d\n", *it);
    }
}

int64_t getTime()
{
    struct timeval tm;
    memset(&tm, 0, sizeof tm);
    if(gettimeofday(&tm, NULL) == -1)
        ERR_EXIT("gettimeofday");
    int64_t t = 0;
    t += tm.tv_sec * 1000 * 1000;
    t += tm.tv_usec;
    return t;
}











