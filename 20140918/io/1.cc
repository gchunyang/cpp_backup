#include <iostream>
#include <string>
#include <vector>
using namespace std;

void printCin()
{
    cout << "bad = " << cin.bad() << endl;
    cout << "fail = " << cin.fail() << endl;
    cout << "eof = " << cin.eof() << endl;
    cout << "good = " << cin.good() << endl;
}

int main(int argc, const char *argv[])
{
    int num;

    printCin();

    while(cin >> num)
    {
        cout << num << endl;
    }

    printCin();
    
    cin.clear();

    printCin();

    string s = "hello";
    cin >> s;
    cout << s << endl;



    return 0;
}
