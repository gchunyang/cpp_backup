#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include <limits>
using namespace std;

int main(int argc, const char *argv[])
{
    int ival;
    //循环只有遇到输入结束才终止
    while (cin >> ival, !cin.eof()) {
        if (cin.bad())
            throw std::runtime_error("IO stream corrupted");
        if (cin.fail()) {
            std::cerr << "bad data, try again!" << std::endl;
            cin.clear(); 
            cin.ignore(std::numeric_limits < std::streamsize > ::max(), '\n');
            continue;
        }

        cout << "val = " << ival << endl;
    } 
    return 0;
}
