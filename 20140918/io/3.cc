#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdexcept>
using namespace std;

int main(int argc, const char *argv[])
{
    string filename = "text.txt";
    ifstream infile(filename.c_str());
    if(!infile)
        throw runtime_error("文件打开失败");

    string s;
    while(infile >> s)
        cout << s << endl;

    /*
    string line;
    while(getline(infile, line))
        cout << line << endl;
    */
    infile.close();
    return 0;
}
