#include <iostream>
#include <string>
#include <vector>
using namespace std;



//本例演示最简单的构造函数
class Student
{
    public:

        Student(int id, const string &name, int age)
        {
            id_ = id;
            name_ = name;
            age_ = age;
        }


        void set(int id, const string &name, int age)
        {
            id_ = id;
            name_ = name;
            age_ = age;
        }

        int getId() const
        {
            return id_;
        }

        const string &getName() const
        {
            return name_;
        }

        int getAge() const
        {
            return age_;
        }

        void print() const
        {
            cout << "const" << endl;
            cout << "id = " << id_ 
                << " name = " << name_
                << " age = " << age_ << endl;
        }

        void print()
        {
            cout << "non-const" << endl;
            cout << "id = " << id_ 
                << " name = " << name_
                << " age = " << age_ << endl;
        }

    private:
        int id_;
        string name_;
        int age_;
};


int main(int argc, const char *argv[])
{
    Student s(12, "zhangsan", 99);
    s.print(); // non-const

    const Student s2(32424, "lisi", 88);

    return 0;
}










