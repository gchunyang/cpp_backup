#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Student
{
    public:

        Student()
            :id_(0), name_(""), age_(0)
        {
        }


        Student(int id, const string &name, int age)
            :id_(id), name_(name), age_(age)
        {
        }


        void set(int id, const string &name, int age)
        {
            id_ = id;
            name_ = name;
            age_ = age;
        }

        int getId() const
        {
            return id_;
        }

        const string &getName() const
        {
            return name_;
        }

        int getAge() const
        {
            return age_;
        }

        void print() const
        {
            cout << "const" << endl;
            cout << "id = " << id_ 
                << " name = " << name_
                << " age = " << age_ << endl;
        }

        void print()
        {
            cout << "non-const" << endl;
            cout << "id = " << id_ 
                << " name = " << name_
                << " age = " << age_ << endl;
        }

    private:
        int id_;
        string name_;
        int age_;
};


int main(int argc, const char *argv[])
{
    Student s(12, "zhangsan", 23);
    s.print();
    return 0;
}










