#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Student
{
    public:

        Student()
        {  }


        Student(int id, const string &name, int age)
        {
            id_ = id;
            name_ = name;
            age_ = age;
        }

        void set(int id, const string &name, int age)
        {
            id_ = id;
            name_ = name;
            age_ = age;
        }

        int getId() const
        {
            return id_;
        }

        const string &getName() const
        {
            return name_;
        }

        int getAge() const
        {
            return age_;
        }

        void print() const
        {
            cout << "const" << endl;
            cout << "id = " << id_ 
                << " name = " << name_
                << " age = " << age_ << endl;
        }

        void print()
        {
            cout << "non-const" << endl;
            cout << "id = " << id_ 
                << " name = " << name_
                << " age = " << age_ << endl;
        }

    private:
        int id_;
        string name_;
        int age_;
};


int main(int argc, const char *argv[])
{
    Student s;
    s.set(12, "zhangsan", 23);
    s.print(); // non-const

    const Student s2(34, "lisi", 32);
    s2.print(); //const


    return 0;
}










