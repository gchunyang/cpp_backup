#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Test
{
    public:
        Test()
        {
            cout << "Construct .." << endl;
        } 

        ~Test()
        {
            cout << "Destruct .. " << endl;
        }
};


int main(int argc, const char *argv[])
{
    Test t; 
    return 0;
}
