#include <iostream>
#include <string>
#include <vector>
using namespace std;

//本例演示了初始化式的顺序问题
class X
{
    public:
        X(int x)
            :j(x), i(j)
        {
        }
    
        void print() const
        {
            cout << "i = " <<  i << " j = " << j << endl;
        }
    private:
        int i;
        int j;
};


int main(int argc, const char *argv[])
{
    X x(99);
    x.print();
    return 0;
}
