#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include <fstream>
#include <algorithm>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif /* __STDC_FORMAT_MACROS */
#include <inttypes.h>
using namespace std;

struct Word
{
    string word_;
    int count_;
};

void readFile(ifstream &in, vector<Word> &words, const vector<string> &stop);
void readStopList(ifstream &in, vector<string> &stop);
bool isStopWord(const string &word, const vector<string> &stop);
void erasePunct(string &s);
void stringToLower(string &s);
void addWordToDict(vector<Word> &words, const string &word);
void printWordFrequency(const vector<Word> &words);
void sortWordsByFrequency(vector<Word> &words);
int64_t getUTime();

int main(int argc, const char *argv[])
{
    //打开文件
    //读取单词
    //打印个数

    if(argc < 2)
    {
        fprintf(stderr, "Usage:%s filename\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    ifstream infile(argv[1]);
    if(!infile)
        throw std::runtime_error("open file error");

    ifstream stopFile("stopList.txt");
    if(!stopFile)
        throw std::runtime_error("stopList does't exist!");


    int64_t startTime = getUTime();

    vector<string> stop;
    readStopList(stopFile, stop);

    vector<Word> words;
    readFile(infile, words, stop);

    int64_t middleTime = getUTime();

    sortWordsByFrequency(words);

    int64_t endTime = getUTime();

    printWordFrequency(words);

    int64_t costByReading = middleTime - startTime;
    int64_t costBySort = endTime - middleTime;

    printf("读取文件花费时间 : %"PRId64"\n，排序所花费的时间 : %"PRId64"\n", costByReading / 1000, costBySort);

    return 0;
}

void readFile(ifstream &in, 
              vector<Word> &words, 
              const vector<string> &stop)
{
    words.clear();
    string word;
    while(in >> word)
    {
        erasePunct(word);
        stringToLower(word);
        if(!isStopWord(word, stop))
            addWordToDict(words, word);
    }
}

void readStopList(ifstream &in, vector<string> &stop)
{
    string word;
    while(in >> word)
        stop.push_back(word);
}


bool isStopWord(const string &word, const vector<string> &stop)
{
    vector<string>::const_iterator it = 
        find(stop.begin(), stop.end(), word);
    return (it != stop.end());
}


void erasePunct(string &s)
{
    string::iterator it = s.begin();
    while(it != s.end())
    {
        if(ispunct(*it))
            it = s.erase(it);
        else
            ++it;
    }
}

void stringToLower(string &s)
{
    for(string::iterator it = s.begin();
        it != s.end();
        ++it)
    {
        if(isupper(*it))
            *it = tolower(*it);
    }
}

void addWordToDict(vector<Word> &words, const string &word)
{
    vector<Word>::iterator it = words.begin();

    while(it != words.end())
    {
        if(word == it->word_)
        {
            ++it->count_;
            break;
        }
        ++it;
    }

    if(it == words.end())
    {
        Word tmp;
        tmp.word_ = word;
        tmp.count_ = 1;
        words.push_back(tmp);
    }
}

void printWordFrequency(const vector<Word> &words)
{
    for(vector<Word>::const_iterator it = words.begin(); 
        it != words.end(); 
        ++it)
    {
        printf("word :%s, frequency: %d\n", it->word_.c_str(), it->count_);
    }
}

static bool cmp(const Word &w1, const Word &w2)
{
    //a > b
    return w1.count_ > w2.count_;
}

void sortWordsByFrequency(vector<Word> &words)
{
    sort(words.begin(), words.end(), cmp);
}



int64_t getUTime()
{
    struct timeval tv;
    ::memset(&tv, 0, sizeof tv);
    if(::gettimeofday(&tv, NULL) == -1)
    {
        throw runtime_error("gettimeofday");
    }
    int64_t t = tv.tv_usec;
    t += tv.tv_sec * 1000 * 1000;
    return t;
}










