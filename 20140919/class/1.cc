#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Student
{
    int num_;
    string name_;
    int age_;

    void print()
    {
        cout << num_ << " " << name_ << " " << age_ << endl;
    }
};


int main(int argc, const char *argv[])
{
    Student s;
    s.num_ = 12;
    s.name_ = "zhangsan";
    s.age_ = 23;
    
    s.print();

    return 0;
}





