#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Student
{
    int num_;
    string name_;
    int age_;

    void print()
    {
        cout << num_ << " " << name_ << " " << age_ << endl;
    }
};


int main(int argc, const char *argv[])
{
    Student s;
    s.num_ = 12;
    s.name_ = "zhangsan";
    s.age_ = 23;
    
    Student *ps = &s;
    ps->print();

    Student s2;
    s2.num_ = 23;
    s2.name_ = "lisi";
    s2.age_ = 22;

    s2.print();



    return 0;
}





