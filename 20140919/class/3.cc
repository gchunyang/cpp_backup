#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Person
{
    private:
        int id_;
        string name_;
        int age_;
    public:
        void setId(int id)
        {
            id_ = id;
        }

        void setName(const string &name)
        {
            name_ = name;
        }

        void setAge(int age)
        {
            age_ = age;
        }

        void print()
        {
            cout << id_ << " " << name_ << " " << age_ << endl;
        }
};


int main(int argc, const char *argv[])
{
    Person p1;
    //p1.id_ = 11;
    p1.setId(11);
    p1.setName("zhangsan");
    p1.setAge(21);

    p1.print();






    return 0;
}
