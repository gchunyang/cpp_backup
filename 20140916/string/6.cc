#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>
using namespace std;

//参数必须使用引用
//1.可以改变字符串
//2.避免字符串复制的开销
void translate(string &s)
{
    for(string::size_type ix = 0;
        ix != s.size(); 
        ++ix)
    {
        if(isupper(s[ix]))
            s[ix] = tolower(s[ix]);
        else if(islower(s[ix]))
            s[ix] = toupper(s[ix]);
    }
}

int main(int argc, const char *argv[])
{
    string s = "helLoWORld";
    translate(s);
    cout << "S = " << s << endl;
    return 0;
}
