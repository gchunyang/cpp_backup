#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    string s = "helloworldfoobar";
    char c;

    while(cin >> c)
    {
        string::size_type pos = s.find(c);
        if(pos == string::npos)
            cout << "not found .." << endl;
        else
            cout << "pos = " << pos << endl;
    }
    return 0;
}
