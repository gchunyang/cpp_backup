#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    string s = "hello";
    s = s + "World";
    string s2 = "foo";
    s += s2;

    cout << s << endl; 

    string s3 = s + "hello" + s2;
    cout << s3 << endl;

    string s4 = "hello" + s + "world";
    cout << s4 << endl;

    string s5 = "hello" + "world" + s3;
    cout << s5 << endl;
    return 0;
}
