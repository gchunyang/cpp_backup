#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    string line;
    //getline读取的string不包含换行符
    while(getline(cin, line))
    {
        cout << line << endl;
    }
    return 0;
}
