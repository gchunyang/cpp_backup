#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
using namespace std;

int main(int argc, const char *argv[])
{
    string s = "hello";
    //这里的C风格字符串只读，不可写
    //这个C风格字符串随时可能失效
    const char *str = s.c_str();
    printf("%s\n", str);
    printf("%s\n", s.c_str());
    return 0;
}
