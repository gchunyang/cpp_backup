#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    string s = "helloworldfoobar";
    string word;

    while(cin >> word)
    {
        string::size_type pos = s.find(word);
        if(pos == string::npos)
            cout << "not found .." << endl;
        else
            cout << "pos = " << pos << endl;
    }
    return 0;
}
