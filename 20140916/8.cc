#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main(int argc, const char *argv[])
{
    int i;
    int sum = 0;
    while(cin >> i)
    {
        cout << i << endl;
        sum += i;
    }

    cout << "Sum : " << sum << endl;
    return 0;
}
