#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    vector<int> vec; //空数组 int类型

    vec.push_back(12); //追加元素
    vec.push_back(4);
    vec.push_back(6);
    vec.push_back(7);
    vec.push_back(9);
    vec.push_back(34);

    for(vector<int>::size_type ix = 0;
        ix != vec.size();
        ++ix)
    {
        cout << "i = " << ix << " vec: " << vec[ix] << endl;
    }

    return 0;
}
