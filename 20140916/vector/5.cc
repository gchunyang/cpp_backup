#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    vector<string> vec;
    vec.push_back("beijing");
    vec.push_back("tianjin");
    vec.push_back("shanghai");
    vec.push_back("hangzhou");

    for(vector<string>::iterator it = vec.begin();
        it != vec.end();
        ++it)
    {
        cout << *it << endl;
    }


    return 0;
}
