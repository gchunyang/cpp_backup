#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    string s = "hello world foobar";

    for(string::iterator it = s.begin();
        it != s.end();
        ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    return 0;
}
