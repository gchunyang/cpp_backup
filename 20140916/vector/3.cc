#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    //大小为10，每个默认为0
    vector<int> vec(10);

    for(int i = 0; i != 10; ++i)
        vec[i] = i;

    vec.push_back(99);
    vec.push_back(45);
    vec.push_back(123);

    for(vector<int>::size_type ix = 0;
        ix != vec.size();
        ++ix)
    {
        cout << vec[ix] << endl;
    }

    return 0;
}
