#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    vector<string> vec;

    string s = "foo";
    vec.push_back(s);

    s = "bar";
    cout << vec[0] << endl;
    return 0;
}
