#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    vector<string> vec;
    vec.push_back("beijing");
    vec.push_back("tianjin");
    vec.push_back("shanghai");
    vec.push_back("hangzhou");

    vector<string>::iterator it = vec.begin();
    while(it != vec.end())
    {
        cout << *it << endl;
        ++it;
    }

    return 0;
}
