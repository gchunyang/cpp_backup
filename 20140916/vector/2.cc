#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    //输入多个字符串，保存在数组中 
    vector<string> dict;
    string s;

    while(cin >> s)
    {
        dict.push_back(s);
    }

    cout << "---------" << endl;
    for(vector<string>::size_type ix = 0;
        ix != dict.size();
        ++ix)
    {
        cout << dict[ix] << endl;
    }

    return 0;
}
