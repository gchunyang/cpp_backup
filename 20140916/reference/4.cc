#include <iostream>
#include <string>
#include <vector>
using namespace std;

void swap(int &a, int &b)
{
    int temp = a;
    a = b;
    b = temp;
}

int main(int argc, const char *argv[])
{
    int a = 10;
    int b = 34;
    swap(a, b);
    cout << "A = " << a << endl;
    cout << "B = " << b << endl;
    return 0;
}
