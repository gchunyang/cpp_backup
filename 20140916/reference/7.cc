#include <iostream>
#include <string>
#include <vector>
using namespace std;

template <typename T>
void swap(T &a, T &b)
{
    T temp(a);
    a = b;
    b = temp;
}


int main(int argc, const char *argv[])
{
    int a,b;
    int *pa = &a;
    int *pb = &b;
    cout << "Before ... " << endl;
    cout << "pa = " << pa << " pb = " << pb << endl;
    ::swap(pa, pb);
    cout << "After" << endl;
    cout << "pa = " << pa << " pb = " << pb << endl;
    return 0;
}
