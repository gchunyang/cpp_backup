#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    int a = 10;
    
    int &b = a; //给变量a取别名叫做b
    int c = a; //利用a初始化一个新的变量c

    b = 20;
    cout << "a = " << a << endl;

    return 0;
}
