#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    int a = 10;
    int &b = a;
    int &c = b; //这里用a或者b初始化无区别

    //此时a有两个别名b、c 一共有3个名称
    return 0;
}
