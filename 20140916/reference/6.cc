#include <iostream>
#include <string>
#include <vector>
using namespace std;

void swap(int *&a, int *&b)
{
    int *temp = a;
    a = b;
    b = temp;
}


int main(int argc, const char *argv[])
{
    int a = 10;
    int b = 20;
    int *pa = &a;
    int *pb = &b;
    cout << "Before ... " << endl;
    cout << "pa = " << pa << " pb = " << pb << endl;
    swap(pa, pb);
    cout << "After" << endl;
    cout << "pa = " << pa << " pb = " << pb << endl;
    return 0;
}
