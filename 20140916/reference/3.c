#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//这是一个错误的版本
void swap(int a, int b)
{
    int temp = a;
    a = b;
    b = temp;
}

int main(int argc, const char *argv[])
{
    int a = 10;
    int b = 23;
    swap(a, b);
    return 0;
}
