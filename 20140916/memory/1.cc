#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    //int类型 值为默认值0 申请了一个int单位
    int *p1 = new int;
    cout << *p1 << endl;

    //值为指定的value，申请了一个int单位
    int *p2 = new int(45);
    cout << *p2 << endl;

    //申请了一个长度为30的int数组，值为默认值
    int *p3 = new int[30];
    for(int i = 0; i < 30; ++i)
        cout << p3[i] << " ";
    cout << endl;

    return 0;
}
