#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    string *ps1 = new string; //""
    cout << (*ps1 == "") << endl;

    string *ps2 = new string("hello");
    cout << *ps2 << endl;

    string *ps3 = new string[10];


    delete ps1;
    delete ps2;

    delete[] ps3;

    return 0;
}
