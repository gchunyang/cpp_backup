#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <stdlib.h>
using namespace std;

class Test
{
    public:
        Test(int val) :val_(val) { cout << "Test" << endl; }

        ~Test() { cout << "Test ..." << endl; }

        Test(const Test &t)
            :val_(t.val_)
        {
            cout << "Copy ....." << endl; 
        }

        int val_;
};


int main(int argc, const char *argv[])
{

    Test *pt = (Test *)malloc(3 * sizeof (Test));

    Test t(12);

    uninitialized_fill(pt, pt + 3, t);
    cout << pt[0].val_ << endl;
    

    Test *pt2 = (Test *)malloc(2 * sizeof (Test));
    uninitialized_copy(pt, pt + 2, pt2);

    free(pt);
    free(pt2);


   return 0;
}











