#include <iostream>
#include <string>
#include <vector>
#include <memory>
using namespace std;

class Test
{
    public:
        Test() { cout << "Test" << endl; }
        ~Test() { cout << "Test ..." << endl; }

        Test(const Test &t)
        {
            cout << "Copy ....." << endl; 
        }

    private:
        //Test(const Test &);
        //void operator(const Test &);
};


int main(int argc, const char *argv[])
{
    allocator<Test> alloc;
    Test *pt = alloc.allocate(3); //申请三个单位的Test内存
    //此时pt指向的是原始内存
    {
        alloc.construct(pt, Test()); //构建一个对象，使用默认值
        //调用的是拷贝构造函数
        alloc.construct(pt+1, Test());
        alloc.construct(pt+2, Test());
    }
    alloc.destroy(pt);
    alloc.destroy(pt+1);
    alloc.destroy(pt+2);

    alloc.deallocate(pt, 3);
    return 0;
}











