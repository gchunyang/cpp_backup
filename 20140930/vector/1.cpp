#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    vector<int> vec;
    vec.resize(100);

    cout << "size = " << vec.size() << endl;
    cout << "capacity = " << vec.capacity() << endl;

    vec.clear();

    cout << "size = " << vec.size() << endl;
    cout << "capacity = " << vec.capacity() << endl;

    {
        vector<int> temp; 
        vec.swap(temp);
    }
    cout << "size = " << vec.size() << endl;
    cout << "capacity = " << vec.capacity() << endl;

    return 0;
}
