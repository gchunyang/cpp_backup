#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Score
{
    string name_;
    int score_;
};


int main(int argc, const char *argv[])
{
    vector<Score> scores; //成绩单
    vector<string> names; //及格名单
    int count = 0; //及格人数

    string name;
    int score;

    while(cin >> name >> score)
    {
        Score s;
        s.name_ = name;
        s.score_ = score;
        scores.push_back(s);
    }


    for(vector<Score>::iterator it = scores.begin();
        it != scores.end();
        ++it)
    {
        if(it->score_ >= 60)
        {
            count++;
            names.push_back(it->name_);
        }
    }

    cout << "及格人数: " << count << endl;
    cout << "及格名单 :" << endl;
    for(string s : names)
    {
        cout << s << endl;
    }

    return 0;
}
