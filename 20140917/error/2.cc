#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
using namespace std;

int main(int argc, const char *argv[])
{
    int a, b;
    try
    {
        cin >> a >> b;
        if(b == 0)
            throw runtime_error("除数不能为零!");

        int res = a / b;
        cout << "res = " << res << endl;
    }
    catch(exception &e)
    {
        cout << "异常信息: " << e.what() << endl;
    }
    return 0;
}
