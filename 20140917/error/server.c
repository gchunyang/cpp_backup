#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)


int main(int argc, const char *argv[])
{
    int fd = socket(PF_INET, SOCK_STREAM, 0);
    if(fd == -1)
        ERR_EXIT("socket");

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof addr);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(9981);
    addr.sin_addr.s_addr = inet_addr("192.168.44.136");
    if(bind(fd, (struct sockaddr*)&addr, sizeof addr) == -1)
        ERR_EXIT("bind");

    if(listen(fd, SOMAXCONN) == -1)
        ERR_EXIT("listen");



    return 0;
}
