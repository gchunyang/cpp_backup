#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
using namespace std;

//对异常的捕获处理
int main(int argc, const char *argv[])
{
    try
    {
        throw runtime_error("出现运行期错误");
    }
    catch(exception &e)
    {
        cout << "异常信息:" << e.what() << endl;
    }

    cout << "继续运行" << endl;
    return 0;
}
