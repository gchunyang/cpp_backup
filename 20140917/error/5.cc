#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
using namespace std;

//对于不同的异常可以采取不同的catch块进行捕捉
int main(int argc, const char *argv[])
{
    try
    {
        int i;
        cin >> i;
        if(i == 0)
            throw runtime_error("出现运行期错误");
        else if(i == 1)
            throw invalid_argument("非法参数");
    }
    catch(runtime_error &e)
    {
        cout << "runtime_error :" << e.what() << endl;
    }
    catch(invalid_argument &e)
    {
        cout << "invalid_argument:" << e.what() << endl;
    }

    cout << "继续运行" << endl;
    return 0;
}
