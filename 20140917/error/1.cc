#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
using namespace std;

int main(int argc, const char *argv[])
{
    
    try
    {
        cout << "foo" << endl;
        throw std::runtime_error("exception ...");

        cout << "bar" << endl;
    }
    catch(...)
    {
        cout << "In catch .." << endl; 
    }

    cout << "continue ..." << endl;
    
    return 0;
}


