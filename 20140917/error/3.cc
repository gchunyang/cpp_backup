#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
using namespace std;

int main(int argc, const char *argv[])
{
    cout << "Before ..." << endl;

    //throw exception();
    throw runtime_error("出现异常");

    cout << "After .." << endl;
    return 0;
}
