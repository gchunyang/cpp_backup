#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>
using namespace std;

//利用下标删除特定字符
int main(int argc, const char *argv[])
{
    string s = "abcd.?hello.";

    string::size_type ix = 0;
    while(ix != s.size())
    {
        if(ispunct(s[ix]))
            s = s.erase(ix, 1);
        else
            ++ix;
    }

    cout << s << endl;

    return 0;
}
