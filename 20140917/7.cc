#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;


bool cmp(int a, int b)
{
    return a > b;
}

int main(int argc, const char *argv[])
{
    int a[10] = {0};
    srand(8891);
    for(int i = 0; i != 10; ++i)
    {
        a[i] = rand() % 100;
    }

    sort(a, a+10, cmp);

    for(int i = 0; i != 10; ++i)
    {
        cout << a[i] << " ";
    }
    cout << endl;

    return 0;
}
