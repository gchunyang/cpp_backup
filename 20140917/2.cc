#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
//#include <cstdio>  std::printf
using namespace std;

int stringToInteger(const string &s)
{
    return atoi(s.c_str());
}

string integerToString(int a)
{
    char text[1024] = {0};
    snprintf(text, sizeof text, "%d", a);
    return string(text);
}

int main(int argc, const char *argv[])
{
    int i;
    while(cin >> i)
    {
        string res = integerToString(i);
        cout << res << " " << res.size() << endl;
    }
    return 0;
}


