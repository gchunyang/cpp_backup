#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
using namespace std;

inline void swap(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}


void bubbleSort(vector<int> &vec, bool (*cmp) (int, int))
{
    for(size_t ix = 0; ix < vec.size() - 1; ++ix)
    {
        for(size_t iy = 0; iy < vec.size()-ix-1; ++iy)
        {
            if(!cmp(vec[iy], vec[iy+1]))
                swap(vec[iy], vec[iy+1]);
        }
    }
}

void print_vec(const vector<int> &vec)
{
    for(int t : vec)
    {
        cout << t << " ";
    }
    cout << endl;

}

bool cmp(int a, int b)
{
    return a < b;
}

bool cmp2(int a, int b)
{
    return a > b;
}

int main(int argc, const char *argv[])
{
    vector<int> vec;
    srand(9981);
    for(int i = 0; i != 10; ++i)
    {
        vec.push_back(rand() % 100);
    }

    print_vec(vec);
    bubbleSort(vec, cmp); //升序排列
    print_vec(vec);


    bubbleSort(vec, cmp2); //降序排列
    print_vec(vec);
    return 0;
}






