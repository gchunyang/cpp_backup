#include <iostream>
#include <string>
#include <vector>
using namespace std;

//这是一个错误的示例，意味着函数重载不包括函数的返回值
int sum(int a, int b)
{
    return a + b; 
}

double sum(int a, int b)
{
    return a + b;
}

int main(int argc, const char *argv[])
{
    int s = sum(1, 2);
    double d = sum(1, 2);
    return 0;
}

