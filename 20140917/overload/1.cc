#include <iostream>
#include <string>
#include <vector>
using namespace std;

int sum(int a, int b)
{
    return a + b;
}

int sum(int a, int b, int c)
{
    return a + b + c;
}

string sum(const string &s1, const string &s2)
{
    return s1 + s2;
}


int main(int argc, const char *argv[])
{
    cout << sum(1, 2) << endl;
    cout << sum(1, 2, 3) << endl;
    cout << sum("hello", "world") << endl;
    return 0;
}

