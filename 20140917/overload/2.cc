#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdlib.h>
using namespace std;

bool cmp(int a, int b)
{
    return a > b;
}

int main(int argc, const char *argv[])
{
    srand(9999);
    vector<int> vec;
    for(int i = 0; i != 100; ++i)
        vec.push_back(rand() % 100);

    sort(vec.begin(), vec.end(), cmp);

    for(vector<int>::iterator it = vec.begin(); it != vec.end(); ++it){
        cout << *it << " ";
    }
    cout << endl;
    return 0;
}
