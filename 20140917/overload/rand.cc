#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)
using namespace std;

void writeIntegerToFile(int fd, int value)
{
    char text[100] = {0};
    snprintf(text, sizeof text, "%d\n", value);
    if(write(fd, text, strlen(text)) == -1)
        ERR_EXIT("write");
}

//返回s
double get_time()
{
    struct timeval tm;
    memset(&tm, 0, sizeof tm);
    if(gettimeofday(&tm, NULL) == -1)
        ERR_EXIT("gettimeofday");
    double res = 0.0;
    res += tm.tv_sec;
    res += tm.tv_usec / (double)1000000;
    return res; 
}

int main(int argc, const char *argv[])
{
    //time_t startTime = time(NULL);
    double startTime = get_time();
    const int kSize = 1000000;
    srand(kSize);
    int fd = open("largeT.txt", O_CREAT | O_WRONLY | O_TRUNC, 0666);
    if(fd == -1)
        ERR_EXIT("open");


    for(int i = 0; i != kSize; ++i)
    {
        writeIntegerToFile(fd, rand() % kSize); 
    }

    close(fd);

    //time_t endTime = time(NULL);
    double endTime = get_time();
    double cost = endTime - startTime;
    cout << "花费时间 " << cost << " s" << endl;
    return 0;
}
