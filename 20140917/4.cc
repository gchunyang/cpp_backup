#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>
using namespace std;

//利用下标删除特定字符
int main(int argc, const char *argv[])
{
    string s = "abcd.?hello.";

    string::iterator it = s.begin();
    while(it != s.end())
    {
        if(ispunct(*it))
            it = s.erase(it); //这里务必对it重新赋值
        else
            ++it;
    }

    cout << s << endl;

    return 0;
}
