#include <iostream>
#include <string>
#include <vector>
using namespace std;

//函数返回值为value
string test()
{
    string s("hello");
    return s;
}


int main(int argc, const char *argv[])
{
    string s = test(); 
    cout << s << endl;
    return 0;
}
