#include <iostream>
#include <string>
#include <vector>
using namespace std;

inline void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main(int argc, const char *argv[])
{
    int a = 10;
    int b = 9;
    swap(&a, &b);
    return 0;
}
