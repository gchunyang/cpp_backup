#include <iostream>
#include <string>
#include <vector>
using namespace std;

//这里的返回值必须为const
const string &test(const string &s)
{
    cout << s << endl;

    //以下的修改无意义
    const char *str = &s[0];
    char *str2 = const_cast<char*>(str);
    *str2 = 'w';
    cout << s << endl;
    return s;
}


int main(int argc, const char *argv[])
{
    test("foo"); 
    return 0;
}
