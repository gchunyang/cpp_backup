#include <iostream>
#include <string>
#include <vector>
using namespace std;

#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)

#define XXX(m) {a, b, c}

int main(int argc, const char *argv[])
{
    XXX("hello"); 
    return 0;
}
