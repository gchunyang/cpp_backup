#include <iostream>
#include <string>
#include <vector>
using namespace std;

//返回值也可以是引用
const string &isShort(const string &s1, const string &s2)
{
    return (s1.size() < s2.size()) ? s1 : s2;
}

int main(int argc, const char *argv[])
{
    cout << isShort("hello", "foo") << endl; 
    return 0;
}
