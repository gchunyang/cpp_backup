#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Test
{
    public:
        Test()
        {
            count++;        
        }

        ~Test()
        {
            count--;
        }

        static void print()
        {
            cout << "当前对象个数: " << count << endl;
        }
    private:
        static int count; //表示对象的个数
};

int Test::count = 0;

int main(int argc, const char *argv[])
{
    Test::print();
    Test t1;
    t1.print();
    Test t2;
    t1.print();
    t2.print();
    
    return 0;
}










