#include <iostream>
#include <string>
#include <vector>
using namespace std;

/*
 * 本例错误的原因是：set系列函数返回的是对象
 * 所以返回时产生了一个临时的对象
 */
class Student
{
    public:
        Student()
            :id_(0), name_(""), age_(0)
        {
            cout << "构造Student" << endl;
        }

        ~Student()
        {
            cout << "销毁Student" << endl;
        }


        Student setId(int id)
        {
            id_ = id;
            return *this;
        }

        Student setName(const string name)
        {
            name_ = name;
            return *this;
        }

        Student setAge(int age)
        {
            age_ = age;
            return *this;
        }

        void print(ostream &os) const
        {
            os << id_ << " " << name_ << " " << age_ << endl;
        }

    private:
        int id_;
        string name_;
        int age_;
};


int main(int argc, const char *argv[])
{
    Student s;
    s.setId(11).setName("zhangsan").setAge(88).print(cout);
    s.print(cout);
    return 0;
}













