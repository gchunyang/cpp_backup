#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    vector<int> vec;
    vec.push_back(12);

    cout << vec.size() << endl; //1
    vec.resize(5);
    cout << vec.size() << endl;

    for(int i : vec)
    {
        cout << i << " ";
    }
    cout << endl;

    return 0;
}
