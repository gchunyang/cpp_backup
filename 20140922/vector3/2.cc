#include <iostream>
#include <string>
#include <vector>
using namespace std;


int main(int argc, const char *argv[])
{
    vector<string> vec;
    vec.push_back("beijing");
    vec.push_back("shanghai");
    vec.push_back("shenzhen");
    vec.push_back("tianjin");
    vec.push_back("nanjing");
    vec.push_back("guangzhou");

    cout << vec.size() << endl; //6

    vec.resize(4);

    for(const string &s : vec)
    {
        cout << s << " ";
    }
    cout << endl;


    return 0;
}
