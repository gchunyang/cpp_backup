#include <iostream>
#include <string>
#include <vector>
using namespace std;

void print(const vector<int> &vec)
{
    cout << "size = " << vec.size() << endl;
    cout << "capacity  = " << vec.capacity() << endl;
}

int main(int argc, const char *argv[])
{
    vector<int> vec;
    print(vec);   // 0 

    for(int i = 0; i != 24; ++i)
    {
        vec.push_back(i);
        print(vec);
    }
    print(vec); // 24  


    vec.reserve(50);
    print(vec);

    while(vec.size() < vec.capacity())
    {
        vec.push_back(99);
    }

    print(vec);

    vec.push_back(23);
    print(vec);


    return 0;
}










