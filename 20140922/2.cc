#include <iostream>
#include <string>
#include <vector>
using namespace std;

/*
 * 本例演示了，this返回自身引用的正确做法
 *
 * 同时说明了重载print函数const版本的必要性
 */
class Student
{
    public:
        Student()
            :id_(0), name_(""), age_(0)
        {
            cout << "构造Student" << endl;
        }

        ~Student()
        {
            cout << "销毁Student" << endl;
        }


        Student &setId(int id)
        {
            id_ = id;
            return *this;
        }

        Student &setName(const string name)
        {
            name_ = name;
            return *this;
        }

        Student &setAge(int age)
        {
            age_ = age;
            return *this;
        }

        const Student &print(ostream &os) const
        {
            os << id_ << " " << name_ << " " << age_ << endl;
            return *this;
        }

        Student &print(ostream &os)
        {
            os << id_ << " " << name_ << " " << age_ << endl;
            return *this;
        }

    private:
        int id_;
        string name_;
        int age_;
};


int main(int argc, const char *argv[])
{
    Student s;
    s.setId(11).print(cout).setName("zhangsan").setAge(88).print(cout);
    s.print(cout);
    return 0;
}













