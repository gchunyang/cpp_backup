#include <iostream>
#include <string>
#include <vector>
using namespace std;

//用一个容器去初始化另一个容器
int main(int argc, const char *argv[])
{
    vector<int> vec;
    vec.push_back(12);
    vec.push_back(89);
    vec.push_back(34);
    vec.push_back(23);
    vec.push_back(56);

    vector<int> vec2(vec);
    for(int t : vec2)
    {
        cout << t << " ";
    }
    cout << endl;

    return 0;
}
