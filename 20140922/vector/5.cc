#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
using namespace std;

//用一段迭代器范围去初始化另一个容器
int main(int argc, const char *argv[])
{
    vector<int> vec;
    vec.push_back(12);
    vec.push_back(89);
    vec.push_back(34);
    vec.push_back(23);
    vec.push_back(56);

    list<double> lst(vec.begin(), vec.end());
    for(double d : lst)
    {
        cout << d << " ";
    }
    cout << endl;

    return 0;
}
