#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

//指针可以当做迭代器
//
int main(int argc, const char *argv[])
{
    int a[5] = {2, 3, 1, 0, 9};

    vector<int> vec(a, a + 3);
    for(int i : vec)
    {
        cout << i << " ";
    }
    cout << endl;


    return 0;
}
