#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

//用一段迭代器范围去初始化另一个容器
int main(int argc, const char *argv[])
{
    vector<int> vec;
    vec.push_back(12);
    vec.push_back(89);
    vec.push_back(34);
    vec.push_back(23);
    vec.push_back(56);

    vector<double> vec2(vec); //vector<int> 与vector<double>类型不同


    return 0;
}
