#include <iostream>
#include <string>
#include <vector>
#include <list>
using namespace std;

int main(int argc, const char *argv[])
{
    list<string> lst;
    lst.push_back("beijing");
    lst.push_back("shanghai");

    lst.push_front("shenzhen");

    for(list<string>::const_iterator it = lst.begin();
        it != lst.end();
        ++it)
    {
        cout << *it << " ";
    }

    cout << endl;
    return 0;
}
