#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

//指针可以当做迭代器
//
int main(int argc, const char *argv[])
{
    int a[5] = {2, 3, 1, 0, 9};
    int *pos = find(a, a + 5, 1);
    cout << pos << endl;
    cout << &a[2] << endl;
    return 0;
}
