#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

//用一段迭代器范围去初始化另一个容器
int main(int argc, const char *argv[])
{
    vector<int> vec;
    vec.push_back(12);
    vec.push_back(89);
    vec.push_back(34);
    vec.push_back(23);
    vec.push_back(56);

    vector<int>::iterator it1, it2;
    it1 = vec.begin(); //12
    it2 = find(vec.begin(), vec.end(), 23);

    vector<int> vec2(it1, it2);
    for(int i : vec2)
    {
        cout << i << " ";
    }
    cout << endl;
    return 0;
}
