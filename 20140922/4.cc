#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Student
{
    public:
        Student();
        Student(int id, const string name, int age);
        void print() const;
    private:
        int id_;
        string name_;
        int age_;
};

Student::Student()
    :id_(0),
     name_(""),
     age_(0)
{
}

Student::Student(int id,
                 const string &name,
                 int age)
    :id_(id),
     name_(name),
     age_(age)
{
    
}

void Student::print() const
{
    cout << "hello" << endl;
}










