#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Test
{
    public:
        friend class Other; //声明Other是Test的朋友
        friend void bar(const Test &t);
    private:
        int x_;
        int y_;
};

class Other
{
    public:
        void foo(Test &t)
        {
            t.x_ = 10;
            t.y_ = 20;
        }
};

void bar(const Test &t)
{
    cout << t.x_ << endl;
}

int main(int argc, const char *argv[])
{
    Test t;
    return 0;
}
