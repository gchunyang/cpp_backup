#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int main(int argc, const char *argv[])
{
    vector<string> vec;
    vec.push_back("beijing");
    vec.push_back("shanghai");
    vec.push_back("tianjin");
    vec.push_back("shenzhen");
    vec.push_back("changchun");

    for(vector<string>::iterator it = vec.begin(); it != vec.end(); ++it){
        cout << *it << " ";    
    }
    cout << endl;

    vector<string>::iterator it1, it2;
    it1 = find(vec.begin(), vec.end(), "shanghai");

    it2 = find(vec.begin(), vec.end(), "shenzhen");

    vec.erase(it1, it2);


    for(vector<string>::iterator it = vec.begin(); it != vec.end(); ++it){
        cout << *it << " ";    
    }                    
    cout << endl;




    return 0;
}
