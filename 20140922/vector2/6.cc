#include <iostream>
#include <string>
#include <vector>
using namespace std;

void print(const vector<int> &lst)
{
    for(int t : lst)
    {
        cout << t << " ";
    }
    cout << endl;
}

int main(int argc, const char *argv[])
{
    vector<int> vec;
    srand(23456);

    for(int i = 0; i != 20; ++i)
    {
        vec.push_back(rand() % 100);
    }
    print(vec);

    vector<int>::iterator it = vec.begin();
    while(it != vec.end())
    {
        if(*it % 2 == 0)
            //vec.erase(it);
            it = vec.erase(it);
        else
            ++it;
    }


    print(vec);



    return 0;
}
