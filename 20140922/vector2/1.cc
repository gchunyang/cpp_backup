#include <iostream>
#include <string>
#include <vector>
using namespace std;

//Test不支持复制和赋值。所以不能放入vector
class Test
{
    public:
        Test() {}

    private:
        //设为私有，禁用了Test的复制和赋值能力 
        Test(const Test &); //用于复制
        void operator=(const Test &); //用于赋值
};

int main(int argc, const char *argv[])
{
    vector<Test> vec;
    Test t;
    vec.push_back(t);
    return 0;
}





