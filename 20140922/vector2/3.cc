#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <algorithm>
using namespace std;


void print(const list<string> &lst)
{
    for(const string &s : lst)
    {
        cout << s << " ";
    }
    cout << endl;
}

int main(int argc, const char *argv[])
{
    list<string> lst;
    lst.push_back("hangzhou");
    lst.push_front("tianjin");
    lst.push_front("shanghai");

    print(lst);
    
    lst.insert(lst.begin(), "shenzhen");
    print(lst);

    lst.insert(lst.end(), "longhua");
    print(lst);

    
    list<string>::iterator it = find(lst.begin(), lst.end(), "tianjin");
    lst.insert(it, "fenghua");
    print(lst);


    return 0;
}










