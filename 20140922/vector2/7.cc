#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
using namespace std;

void print(const list<int> &lst)
{
    for(int t : lst)
    {
        cout << t << " ";
    }
    cout << endl;
}

int main(int argc, const char *argv[])
{
    list<int> lst;
    srand(23456);

    for(int i = 0; i != 20; ++i)
    {
        lst.push_back(rand() % 100);
    }
    print(lst);

    list<int>::iterator it = lst.begin();
    while(it != lst.end())
    {
        if(*it % 2 == 0)
            //lst.erase(it);
            it = lst.erase(it);
        else
            ++it;
    }


    print(lst);



    return 0;
}
