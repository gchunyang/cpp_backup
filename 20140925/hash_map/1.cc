#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
using namespace std;


int main(int argc, const char *argv[])
{
    unordered_map<string, int> m;

    m["beijing"] = 1;
    m["shanghai"] = 2;
    m["shenzhen"] = 3;

    for(unordered_map<string, int>::const_iterator it = m.begin();
        it != m.end();
        ++it)
    {
        cout << it->first << " : " << it->second << endl;
    }
    


    return 0;
}

