#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, const char *argv[])
{
    string s = "helloworld";

    string::reverse_iterator it = s.rbegin();
    while(it != s.rend())
    {
        if(*it == 'r')
        {
            string::iterator tmp = (++it).base();
            tmp = s.erase(tmp);
            it = string::reverse_iterator(tmp);
            //it = string::reverse_iterator(s.erase((++it).base()));
        }
        else
            ++it;
    }


    cout << s << endl;



    return 0;
}
