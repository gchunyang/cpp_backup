#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <ctype.h>
using namespace std;


void print(int i)
{
    cout << i << " ";
}

int main(int argc, const char *argv[])
{
    srand(1000);
    vector<int> vec;
    for(int i = 0; i < 20; ++i)
    {
        vec.push_back(rand() % 50);
    }
    
    for_each(vec.begin(), vec.end(), print);
    cout << endl;

    int min = *std::min_element(vec.begin(), vec.end());
    cout << "min = " << min << endl;

}
