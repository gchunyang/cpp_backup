#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <ctype.h>
using namespace std;

void print(const string &s)
{
    cout << s << " ";
}

bool isShorter(const string &s)
{
    return s.size() < 6;
}

int main(int argc, const char *argv[])
{
    vector<string> vec;
    vec.push_back("beijing");
    vec.push_back("changchun");
    vec.push_back("shijiahzuang");
    vec.push_back("shenyang");
    vec.push_back("dalian");
    vec.push_back("jinan");
    vec.push_back("nanjing");

    vector<string>::iterator it = 
        std::find(vec.begin(), vec.end(), "dalian");
    cout << *it << endl;
    //find_if

    it = std::find_if(vec.begin(), vec.end(), isShorter);
    cout << *it << endl;

    return 0;
}
