#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <ctype.h>
using namespace std;


void print(const string &s)
{
    cout << s << " ";
}

//bool isAAA(element) 谓词
bool comp(const string &s1, const string &s2)
{
    return s1.size() < s2.size();
}

int main(int argc, const char *argv[])
{
    vector<string> vec;
    vec.push_back("beijing");
    vec.push_back("changchun");
    vec.push_back("shijiahzuang");
    vec.push_back("shenyang");
    vec.push_back("dalian");
    vec.push_back("jinan");
    vec.push_back("nanjing");

    for_each(vec.begin(), vec.end(), print);
    cout << endl;

    vector<string>::const_iterator it = 
        max_element(vec.begin(), vec.end(), comp);
    cout << *it << endl;
    
    return 0;
}
