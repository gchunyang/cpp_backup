#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <ctype.h>
using namespace std;


void print(const string &s)
{
    cout << s << " ";
}

int main(int argc, const char *argv[])
{
    vector<string> vec;
    vec.push_back("beijing");
    vec.push_back("changchun");
    vec.push_back("shijiahzuang");
    vec.push_back("shenyang");
    vec.push_back("dalian");
    vec.push_back("jinan");
    vec.push_back("nanjing");


    list<string> lst;

    std::copy(vec.begin(), vec.end(), back_inserter(lst)); //push_back

    for_each(lst.begin(), lst.end(), print); 
    cout << endl;

    lst.clear();

    std::copy(vec.begin(), vec.end(), front_inserter(lst)); //push_front

    for_each(lst.begin(), lst.end(), print); 
    cout << endl;
    return 0;
}
