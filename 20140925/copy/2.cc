#include <iostream>
#include <vector>
#include <string.h>
using namespace std;


class String
{
    public:
        String();
        String(const char *s);
        ~String();


        void print() const
        {
            cout << str_ << endl;
        }

    private:
        char *str_;
};

String::String()
    :str_(new char(0))
{ }

String::String(const char *s)
    :str_(new char[strlen(s) + 1])
{
    ::strcpy(str_, s);
}


String::~String()
{
    delete str_; 
}





int main(int argc, const char *argv[])
{
    String s("hello");   
    s.print();
    return 0;
}
