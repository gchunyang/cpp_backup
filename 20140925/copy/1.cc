#include <iostream>
#include <string>
#include <vector>
using namespace std;


class Student
{

    public:
        Student() {}
        Student(int id, const string &name, int age)
            :id_(id), name_(name), age_(age)
        {  }
        Student(const Student &other)
            :id_(other.id_),
             name_(other.name_),
             age_(other.age_)
        {
        }

        void print() const
        {
            cout << id_ << " : " << name_ << " : " << age_;
        }

    private:
        int id_;
        string name_;
        int age_;
};


int main(int argc, const char *argv[])
{
    Student s(11, "zhangsan", 23);

    s.print();


    Student s2(s);
    s2.print();













    return 0;
}

