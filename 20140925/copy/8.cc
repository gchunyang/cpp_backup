#include <iostream>
#include <string>
#include <vector>
using namespace std;


#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&); \
    void operator=(const TypeName&)

class Test
{
    public:
        Test() {}
        ~Test() {}
    private:
        DISALLOW_COPY_AND_ASSIGN(Test);
};



int main(int argc, const char *argv[])
{
    Test t;

    Test t2(t);
    
    Test t3;
    t3 = t;

    return 0;
}
