#include <iostream>
#include <string>
#include <vector>
using namespace std;

template <typename T>
T max(T a, T b)
{
    return a > b ? a : b;
}


int main(int argc, const char *argv[])
{
    string s = "hello";

    ::max("hello", "world");
    ::max("apple", "orange");
    ::max("apple", s);

    return 0;
}


