#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Animal
{
    public:
        virtual void run() = 0; //纯虚函数
};

class Cat : public Animal
{
    public:
        /*
        void run()
        {
            cout << "..." << endl;
        }*/
};



int main(int argc, const char *argv[])
{
    Cat c;
    return 0;
}
