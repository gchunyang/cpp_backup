#ifndef SYSUTIL_H_
#define SYSUTIL_H_

#include <stdint.h>
#include <stddef.h>

void nano_sleep(double val);
ssize_t readn(int fd, void *buf, size_t count);
ssize_t writen(int fd, const void *buf, size_t count);
ssize_t readline(int fd, void *usrbuf, size_t maxlen);
void send_int32(int sockfd, int32_t val);
int32_t recv_int32(int sockfd);

#endif //SYSUTIL_H_