#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <errno.h>
#define ERR_EXIT(m) \
    do { \
        perror(m);\
        exit(EXIT_FAILURE);\
    }while(0)

#define BUFFERSIZE 65536

typedef struct
{
    int fd_; //fd
    int cnt_; //缓冲区可用的字节数
    char *ptr_; //指向缓冲区可用的第一个字节
    char buffer_[BUFFERSIZE]; //缓冲区
} rio_t;

//初始化IO系统
void rio_init(rio_t *rp, int fd)
{
    rp->fd_ = fd;
    rp->cnt_ = 0;
    rp->ptr_ = rp->buffer_;
    memset(rp->buffer_, 0, BUFFERSIZE);
}

//用来替换read
ssize_t rio_read(rio_t *rp, char *usrbuf, size_t n)
{

    //缓冲区为空时，执行read操作
    while(rp->cnt_ <= 0)
    {
        ssize_t nread = read(rp->fd_, rp->buffer_, BUFFERSIZE);
        if(nread == -1)
        {
            if(errno == EINTR)
                continue;
            return -1;  //ERROR
        }
        else if(nread == 0)
            return 0;

        //正常读取
        rp->cnt_ = nread;
        rp->ptr_ = rp->buffer_; //重置指针
    }

    //现有库存和用户要求的数量 取较小者
    int cnt = (rp->cnt_ < n) ? rp->cnt_ : n;
    memcpy(usrbuf, rp->ptr_, cnt);
    rp->ptr_ += cnt;
    rp->cnt_ -= cnt;

    return cnt;  //成功读取的字节数
}


ssize_t rio_readn(rio_t *rp, void *buf, size_t count)
{
    size_t nleft = count;  //剩余的字节数
    ssize_t nread; //用作返回值
    char *bufp = (char*)buf; //缓冲区的偏移量

    while(nleft > 0)
    {
        //不再执行read系统调用
        nread = rio_read(rp, bufp, nleft);
        if(nread == -1)
        {
            if(errno == EINTR)
                continue;
            return -1; // ERROR
        }
        else if(nread == 0) //EOF
            break;

        nleft -= nread;
        bufp += nread;
    }

    return (count - nleft);
}

ssize_t rio_readline(rio_t *rp, char *usrbuf, size_t maxlen)
{
    int i; //计数
    int nread;

    char *bufp = usrbuf;
    char c; //暂存字符

    for(i = 0; i < maxlen - 1; ++i)
    {
        if((nread = rio_read(rp, &c, 1)) == -1)
            return -1;
        else if(nread == 0) //EOF
        {
            if(i == 0)
                return 0;
            break;
        }

        *bufp++ = c; //放入usrbuf
        if(c == '\n') //碰到换行符直接退出循环
            break; 
    }
    *bufp = '\0';
    return i; //返回读取的字节数 
}


ssize_t rio_writen(int fd, const void *buf, size_t count)
{
    size_t nleft = count;
    ssize_t nwrite;
    const char *bufp = (const char*)buf;
    
    while(nleft > 0)
    {
        nwrite = write(fd, bufp, nleft);
        if(nwrite <= 0) // ERROR
        {
            if(nwrite == -1 && errno == EINTR)
                continue;
            return -1;
        }

        nleft -= nwrite;
        bufp += nwrite;
    }
    
    return count;
}


int main(int argc, const char *argv[])
{
    int fd = open("test.txt", O_RDONLY);
    if(fd == -1)
        ERR_EXIT("open test.txt");
    rio_t rio;
    rio_init(&rio, fd);

    char buf[1024] = {0};
    // rio_readn(&rio, buf, 3);
    // printf("%s\n", buf);

    while(rio_readline(&rio, buf, sizeof buf) > 0)
    {
        printf("%s", buf);
    }

    close(fd);

    return 0;
}

